## [1.0.2] - 31 Mar 2019

* fixed some naming conventions and formats for pub


## [1.0.0] - 31 Mar 2019

* initial release.
